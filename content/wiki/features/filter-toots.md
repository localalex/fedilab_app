*[Home](../../home) > [Features](../../features) &gt;* Filter toots

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilb</b>
</div>

## Filter Toots
You can filter toots in your timelines.

#### Home timeline
 Long press on home timeline tab icon to open the filter menu. There are three options available

 <img src="../../res/filters/home_filters_menu.png" width="300px">

- **Show boosts**<br>
*If you uncheck this, boosts will be hidden from your home timeline*

- **Show replies**<br>
*If you uncheck this, replies will be hidden from your home timeline*

- **Regular expression**<br>
*The regular expression will discard toots containing specific words. These words need to be divided by a pipe character "|".*
  <br>
  <img src="../../res/filters/home_filters_current_dialog.png" width="300px">
  <br><br>
  *At any time, you can check what words are used to filter your time line.*
  <br>
  <img src="../../res/filters/home_filters_current_menu.png" width="300px">


#### Local & federated timelines

Long press on timeline icon to open the filter menu. In these timelines, you can filter by a regular expression.


#### Notification timeline

There are 4 filters available on notification timeline.

<img src="../../res/filters/notification_filters.png" width="300px">

- **Favorites**<br>
*Notifications when someone add your toots to favorites*

- **New followers**<br>
*Notifications when someone followed you*

- **Mentions**<br>
*Notifications when someone mentioned you*

- **Boosts**<br>
*Notifications when someone boosts toots*



**Profile page**

- You can filter toots on a profile by: pinned toots, media only, hide/show replies & boosts.<br>
Log press on 'TOOTS' main tab to open the menu.
<br>
<img src="../../res/filters/profile_menu_toot.png" width="300px">

- You can also use the 'Hide boosts from' option from top right menu to permanently hide boosts from an account.<br>
<img src="../../res/filters/profile_menu_top.png" width="300px">
