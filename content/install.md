
### Installation Methods<br><br>

- **Install using an App Store**

 - You can install Fedilab using F-Droid for free
<a href="https://f-droid.org/app/fr.gouv.etalab.mastodon" target="_blank" rel="noopener noreferrer"><img alt='Get it on Fdroid' src='../wiki/res/install/fdroid.png' width="200"/></a>

 - If you only use Google Play, you can purchase Fedilab for a small price
<a href="https://play.google.com/store/apps/details?id=app.fedilab.android" target="_blank" rel="noopener noreferrer"><img alt='Get it on Google Play' src='../wiki/res/install/play.png' width="200"/></a>


- **Build from source**<br>
Fedilab is completely OpenSource. So you can build it yourself<br>This method is more suitable for experienced users. But if you want to give it a try, this tutorial might help.<br>
[**How to build Fedilab**](../wiki/build)
